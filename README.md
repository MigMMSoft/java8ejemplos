# Java8Ejemplos

Ejemplos de lo nuevo de java 8 (con respecto a java 7)
Complemento de la presentación [Prezi]( https://prezi.com/view/LRLZklIAn4FNlwKRskkX/)

### Detalle del Sistema

- Java 8
- Maven 4
- IDE Netbeans 8.2

### Detalle de los Ejemplos



```
└───cl
    └───mmsoft
        ├───dataTimeAPI
        │       Ejemplo01Local.java
        │       Ejemplo02Zoned.java
        │       Ejemplo03ChronoUnits.java
        │       Ejemplo04PeriodDuration.java
        │       Ejemplo05TemporalAdjusters.java
        │       Ejemplo06BackwardCompatibility.java
        │
        ├───defaultMethod
        │   │   Ejemplo01DefaultMethod.java
        │   │   SumaDivideInterface.java
        │   │   Sumador.java
        │   │   SumaMultiplica.java
        │   │
        │   └───java7
        │           Ejemplo01DefaultMethod.java
        │           SumaDivideInterface.java
        │           Sumador.java
        │           SumadorAbstract.java
        │           SumadorPrima.java
        │           SumaMultiplica.java
        │
        ├───functionalInterfaces
        │       Ejemplo01Predicate.java
        │       Ejemplo02Function.java
        │       Ejemplo03Supplier.java
        │
        ├───lambdaExpressions
        │       Ejemplo00InplementaBiFunction.java
        │       Ejemplo01InplementaInterface.java
        │       Ejemplo02InplementaInterfaceSinLambda.java
        │       Ejemplo03Lambda.java
        │       Ejemplo04Scope.java
        │       Ejemplo05Recursividad.java
        │       OperacionMatematicaInterface.java
        │
        ├───methodReference
        │       Ejemplo01StaticReference.java
        │       Ejemplo02InstanceReference.java
        │       Ejemplo03NuevaInstancia.java
        │       Ejemplo04DeLaintanciaDeOtroObjeto.java
        │
        ├───pruebaProceso
        │       ProcesaArchivo.java
        │
        └───streams
                Ejemplo01ForEach.java
                Ejemplo02Map.java
                Ejemplo03Filter.java
                Ejemplo04Limit.java
                Ejemplo05Sorted.java
                Ejemplo06ParallelProcessing.java
                Ejemplo07Collectors.java
                Ejemplo08Statistics.java
                Ejemplo09FlatMap.java
```