package cl.mmsoft.streams;

import java.util.Date;
import java.util.Random;

/**
 * ParallelStream es la alternativa del flujo para el procesamiento en
 * paralelo. Eche un vistazo al siguiente segmento de código que imprime la
 * diferencia de tiempo entre proceso normal y paralelo
 * @author MigSoft
 */
public class Ejemplo06ParallelProcessing {

    /**
     * @param args
     */
    public static void main(String args[]) {
        //proceso normal
        Date inicio = new Date();
        Random random = new Random();
        long count = random.ints().
                limit(10000).
                filter(entero -> entero > 0).
                count();
        Date fin = new Date();
        System.out.println("Tiempo 1 (" + count + "): " + (fin.getTime() - inicio.getTime()));
        //proceso paralelo
        inicio = new Date();
        random = new Random();
        count = random.ints().
                parallel().
                limit(10000).
                filter(entero -> entero > 0).
                count();
        fin = new Date();
        System.out.println("Tiempo 2 (" + count + "): " + (fin.getTime() - inicio.getTime()));
    }
}
