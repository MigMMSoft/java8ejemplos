package cl.mmsoft.streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Los colectores se utilizan para combinar el resultado del procesamiento 
 * en los elementos de una corriente. Los recopiladores se pueden utilizar 
 * para devolver una lista o una cadena.
 * @author MigSoft
 */
public class Ejemplo07Collectors {

    /**
     * @param args
     */
    public static void main(String args[]) {
        List<String> strings = Arrays.asList("abc", "", "bc", "efg", "abcd", "", "jkl");
        List<String> filtered = strings.
                stream().
                filter(string -> !string.isEmpty()).
                collect(Collectors.toList());

        System.out.println("Lista Filtrada: " + filtered);
        
        //String mergedString = strings.
          String mergedString = filtered.
                stream().
                //filter(string -> !string.isEmpty()).
                collect(Collectors.joining("; "));
        
        System.out.println("Lista a String: " + mergedString);
    }
}
