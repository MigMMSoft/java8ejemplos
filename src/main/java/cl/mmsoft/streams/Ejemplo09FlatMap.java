package cl.mmsoft.streams;

import static java.util.Arrays.asList;
import java.util.List;
import static java.util.stream.Collectors.toList;
import java.util.stream.Stream;

/**
 * El método flatMap nos permite unir dos listas de elementos.
 * @author mig_s
 */
public class Ejemplo09FlatMap {
    /**
     * @param args 
     */
    public static void main(String args[]) {
        List<Integer> lista1 = asList(3, 2, 2);
        List<Integer> lista2 = asList(3, 7, 3, 5);
                
        List<Integer> lista = Stream.of(lista1,lista2).
            flatMap(numeros -> numeros.stream()).
            collect(toList());

        
        lista.forEach(System.out::println);
    }
}
