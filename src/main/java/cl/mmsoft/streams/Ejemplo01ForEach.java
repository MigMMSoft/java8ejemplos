package cl.mmsoft.streams;

import java.util.Random;

/**
 * Stream ha proporcionado un nuevo método 'forEach' para iterar cada elemento 
 * de la secuencia. El siguiente segmento de código muestra cómo imprimir 10 
 * números aleatorios utilizando forEach.
 * @author MigSoft
 */
public class Ejemplo01ForEach {
    /**
     * @param args 
     */
    public static void main(String args[]) {
        Random random = new Random();
        random.ints().limit(10).forEach(System.out::println);
    }
}
