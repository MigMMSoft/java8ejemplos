package cl.mmsoft.streams;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * El método 'límite' se utiliza para reducir el tamaño del flujo. El 
 * siguiente segmento de código muestra cómo imprimir 10 números aleatorios 
 * usando límite.
 * @author MigSoft
 */
public class Ejemplo04Limit {

    /**
     * @param args
     */
    public static void main(String args[]) {
        Random random = new Random();
        random.ints().
               limit(10).
               forEach(System.out::println);
        
        List<String>strings = Arrays.asList("abc", "2", "bc", "efg", "abcd","", "jkl");
        strings.stream().limit(2).forEach(System.out::println);
    }
}
