package cl.mmsoft.streams;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;

/**
 * Con Java 8, se introducen los recopiladores de estadísticas para calcular
 * todas las estadísticas cuando se procesa el flujo.
 * @author MigSoft
 */
public class Ejemplo08Statistics {

    /**
     * @param args
     */
    public static void main(String args[]) {
        List<Integer> numbers = Arrays.asList(3, 2, 2, 3, 7, 3, 5);

        IntSummaryStatistics stats = numbers.
                stream().
                mapToInt((x) -> x).
                summaryStatistics();

        System.out.println("El numero mayor : " + stats.getMax());
        System.out.println("El numero menor : " + stats.getMin());
        System.out.println("Suma todo los numeros : " + stats.getSum());
        System.out.println("El promedio de los numeros : " + stats.getAverage());
    }
}
