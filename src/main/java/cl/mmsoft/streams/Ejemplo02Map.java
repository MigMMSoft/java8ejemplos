package cl.mmsoft.streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * El método 'mapa' se utiliza para asignar cada elemento a su resultado 
 * correspondiente. El siguiente segmento de código imprime cuadrados únicos 
 * de números usando mapa.
 * @author MigSoft
 */
public class Ejemplo02Map {

    /**
     * @param args
     */
    public static void main(String args[]) {
        List<Integer> numbers = Arrays.asList(3, 2, 2, 3, 7, 3, 5);
        List<Integer> squaresList = numbers.
                                    stream().
                                    distinct().
                                    map(i -> i * i).
                                    collect(Collectors.toList());
        
        squaresList.forEach(System.out::println);
    }
}
