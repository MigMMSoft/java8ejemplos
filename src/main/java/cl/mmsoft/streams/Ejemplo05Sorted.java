package cl.mmsoft.streams;

import java.util.Random;

/**
 * El método 'ordenado' se utiliza para ordenar el flujo. El siguiente 
 * segmento de código muestra cómo imprimir 10 números aleatorios en un 
 * orden ordenado.
 * @author MigSoft
 */
public class Ejemplo05Sorted {

    /**
     * @param args
     */
    public static void main(String args[]) {
        Random random = new Random();
        random.ints().
                limit(10).
                sorted().
                forEach(System.out::println);
    }
}
