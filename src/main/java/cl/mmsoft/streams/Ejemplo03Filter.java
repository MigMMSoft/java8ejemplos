package cl.mmsoft.streams;

import java.util.Arrays;
import java.util.List;

/**
 * El método 'filter' se utiliza para eliminar elementos basados en un 
 * criterio. El segmento de código siguiente imprime un recuento de cadenas 
 * vacías utilizando filtro.
 * @author MigSoft
 */
public class Ejemplo03Filter {
    /**
     * @param args 
     */
    public static void main(String args[]) {
        List<String>strings = Arrays.asList("abc", "", "bc", "efg", "abcd","", "jkl");
        long count;
        count = strings.
                stream().
                filter((String string) -> !string.isEmpty()).
                count();
        
        System.out.println("Cantidad: " + count);
    }
}
