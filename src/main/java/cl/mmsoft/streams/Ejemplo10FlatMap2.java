package cl.mmsoft.streams;

import java.util.ArrayList;
import java.util.List;

/**
 * El método flatMap nos permite unir dos listas de elementos. En el ejemplo se
 * unen dos listas de una estructura anidada Persona -> Viaje
 *
 * @author mig_s
 */
public class Ejemplo10FlatMap2 {

    public static void main(String args[]) {
        Persona p = new Persona("pedro");
        Viaje v = new Viaje("Francia");
        Viaje v2 = new Viaje("Inglaterra");
        p.addViaje(v);
        p.addViaje(v2);
        Persona p1 = new Persona("gema");
        Viaje v3 = new Viaje("Italia");
        Viaje v4 = new Viaje("Belgica");
        p1.addViaje(v3);
        p1.addViaje(v4);

        List<Persona> lista = new ArrayList<>();
        lista.add(p);
        lista.add(p1);

        //java 8 union con flatMap
        lista.stream()
                .map(persona -> persona.getLista())
                .flatMap(viajes -> viajes.stream())
                //opción  1 juntar propiedad y recorrer e imprimir conjunto
                .map(viaje -> viaje.getPais())
                .forEach(System.out::println);
                //opcion  2 recorrer e imprimir propiedad
                //.forEach(viaje -> System.out.println(viaje.getPais()));
        System.out.println("_________________________________________");
        //java 7
        for (Persona persona : lista) {
            for (Viaje viaje : persona.getLista()) {
                System.out.println(viaje.getPais());
            }
        }
    }
}

class Persona {

    private String nombre;
    private List<Viaje> lista = new ArrayList<>();

    public String getNombre() {
        return nombre;
    }

    public void addViaje(Viaje v) {
        lista.add(v);
    }

    public List<Viaje> getLista() {
        return lista;
    }

    public Persona(String nombre) {
        super();
        this.nombre = nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}

class Viaje {

    private String pais;

    public Viaje(String pais) {
        super();
        this.pais = pais;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }
}
