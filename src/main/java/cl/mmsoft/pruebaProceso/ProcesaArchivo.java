package cl.mmsoft.pruebaProceso;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Prueba de procesamiento de un archivo con un millón de líneas utilizando stream. 
 * Además en esta prueba podemos comprobar que el procesamiento paralelo no siempre es la solución requerida.
 * La operación que se debe realizar es la siguiente:
 * 1. Se deben consolidar todos los periodos de los 6 vectores en un vector de a lo más 23 elementos.
 * 2. Si los periodos se repiten se debe dejar sólo 1.
 * 3. Los periodos se deben ordenar de mayor a menor.
 * 4. La salida debe ser la siguiente:
 *     Encabezado: 9 dígitos que se copian de la entrada
 *     Marca: una letra que puede tener los valores S, N ó D.
 *     Vector: un vector de a lo más 23 periodos.
 * 
 * 5. Se debe considerar lo siguiente:
 *     5.1 Si el vector consolidado tiene más de 23 elementos se debe colocar la marca S y el vector se debe llenar de espacios en blanco.
 *     5.2 Si el vector consolidado tiene cero elementos (porque vienen sólo 0s en los periodos) se debe colocar la marca N.
 *     5.3 Si el vector tiene menos de 24 elementos se debe colocar la marca D.
 * 
 * @author MigSoft
 */
public class ProcesaArchivo {
    private static final int VECTOR = 6;
    private static final int MAX_VECTOR = 23;
    private static final int INICIO_CADENA = 9;
    private static final int LARGO_TOTAL = 148;
    
    //
    public static void main(String args[]) throws IOException {
        ProcesaArchivo p = new ProcesaArchivo();
        String arch = "vector1000000.txt";
        Long inicio = System.nanoTime();
        p.procesaArchivo(arch);
        Long fin = System.nanoTime();
        System.out.println("Tiempo ocupado: " + TimeUnit.SECONDS.convert((fin-inicio), TimeUnit.NANOSECONDS));
    }
    
    public void procesaArchivo(String archivo) throws IOException{
        File salida = new File("procesado"+archivo);
        try (BufferedWriter output = new BufferedWriter(new FileWriter(salida))) {
            Consumer<String> c = (linea) -> {
                try {
                    output.write(linea);
                } catch (IOException ex) {
                    Logger.getLogger(ProcesaArchivo.class.getName()).log(Level.SEVERE, null, ex);
                }
            };
            
            //El proceso paralelo desde este punto hace que el proceso sea más lento
            Path path = FileSystems.getDefault().getPath(archivo);
            Files.lines(path)
                //.parallel()
                .map(ProcesaArchivo::procesarLine)
                .forEach(c);
        }
        
    }
    
    public synchronized static String procesarLine(String linea){
        //rescata secuencia
        StringBuilder salida = new StringBuilder(linea.substring(0, INICIO_CADENA));
        //divide periodos
        int inicio = INICIO_CADENA;
        int fin = INICIO_CADENA + VECTOR;
        ArrayList<String> list = new ArrayList();
        while (linea.length() > fin) {
            String fecha = linea.substring(inicio, fin);
            inicio += VECTOR;
            fin += VECTOR;
            list.add(fecha);
        }
        //filtra y junta
        //El proceso paralelo desde este punto hace 4 beses más lento
        List<String> slist = list
                .stream()
                //.parallel()
                .filter(p->!"000000".equals(p))
                .distinct()
                .collect(Collectors.toList());
        //califica
        long elementos = slist.size();
        if (elementos == 0) {//sin elementos
            salida.append("N");
        }else if(elementos > MAX_VECTOR){
            salida.append("S");
        }else{
            salida.append("D");
            salida.append(slist.stream().sorted().collect(Collectors.joining()));
        }
        //rellena
        while (salida.length() < LARGO_TOTAL) {
            salida.append(" ");
        }

        return salida.append("\n").toString();
    }
}
