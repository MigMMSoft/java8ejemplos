package cl.mmsoft.functionalInterfaces;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * La interfaz de Predicate <T> es una interfaz funcional con una prueba de 
 * método (Object) para devolver un valor booleano. Esta interfaz significa 
 * que un objeto se prueba para ser verdadero o falso.
 * @author MigSoft
 */
public class Ejemplo01Predicate {

    /**
     * @param args
     */
    public static void main(String args[]) {
        Ejemplo01Predicate ejePredicate = new Ejemplo01Predicate();
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);

        // Predicate<Integer> predicate = n -> true
        // n se pasa como parámetro al método de prueba de la interfaz de predicado
        // El método de prueba siempre devolverá true sin importar el valor n.
        System.out.println("Imprimir todos los números: ");

        //Pasar n como parámetro
        ejePredicate.eval(list, n -> true);

        // Predicate<Integer> predicate1 = n -> n%2 == 0
        // n se pasa como parámetro al método de prueba de la interfaz de predicado
        // método de prueba devolverá true si n% 2 llega a ser cero
        System.out.println("Imprimir números pares: ");
        ejePredicate.eval(list, n -> n % 2 == 0);

        // Predicate<Integer> predicate2 = n -> n > 3
        // n se pasa como parámetro al método de prueba de la interfaz de predicado
        // El método de prueba devolverá true si n es mayor que 3.
        System.out.println("Imprime números mayores que 3:");
        ejePredicate.eval(list, n -> n > 3);
    }

    public void eval(List<Integer> list, Predicate<Integer> predicate) {
        //java tradicional
        for (Integer n : list) {
            if (predicate.test(n)) {
                System.out.println(n);
            }
        }
        //java 8
//        list.stream().
//                filter((n) -> (predicate.test(n))).
//                forEach((n) -> {System.out.println(n);});

//        list.stream().
//                filter((n) -> (predicate.test(n))).
//                forEach(System.out::println);
    }
}
