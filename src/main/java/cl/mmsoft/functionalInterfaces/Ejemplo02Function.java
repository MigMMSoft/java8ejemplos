package cl.mmsoft.functionalInterfaces;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

/**
 * Representa una función que acepta un argumento y genera un resultado.
 * Interface Function<T,R>
 * 
 * Parámetros de tipo:
 * T - el tipo de entrada a la función
 * R - el tipo de resultado de la función
 * 
 * @author MigSoft
 */
public class Ejemplo02Function {

    /**
     * @param args 
     */
    public static void main(String args[]) {
        Function<Persona, String> funPersonaToNombres = 
                (Persona e) -> {
            return e.getNombres();
        };
        
        List<Persona> personasList
                = Arrays.asList(new Persona("Tom Jones", 45),
                        new Persona("Harry Major", 25),
                        new Persona("Ethan Hardy", 65),
                        new Persona("Nancy Smith", 15),
                        new Persona("Deborah Sprightly", 29));
        List<String> nombresList = new ArrayList<>();
        
        personasList.forEach((persona) -> {
            nombresList.add(funPersonaToNombres.apply(persona));
        });
        
        nombresList.forEach(System.out::println);
    }

}

class Persona {

    private String nombres;
    private int edad;

    public Persona(String nombres, int edad) {
        this.nombres = nombres;
        this.edad = edad;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

}
