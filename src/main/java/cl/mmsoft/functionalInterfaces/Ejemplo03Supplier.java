package cl.mmsoft.functionalInterfaces;

import java.util.function.Supplier;

/**
 * Interface Supplier<T>
 * 
 * Parámetros de tipo:
 * T - el tipo de resultados suministrados por este proveedor
 * @author MigSoft
 */
public class Ejemplo03Supplier {

    /**
     * @param args 
     */
    public static void main(String args[]) {
        Supplier<Humano> generadorHumanos = Humano::new;
        
        //Humano uno
        Humano humano1 = generadorHumanos.get();
        humano1.setEdad(12);
        humano1.setNombres("Harry Major");
        //Gemelo
        Humano humano2 = generadorHumanos.get();
        humano2.setEdad(12);
        humano2.setNombres("Harry Major");
        
        System.out.println("Hascode: " + humano1.hashCode() + " -> " + humano1);
        System.out.println("Hascode: " + humano2.hashCode() + " -> " + humano2);
        System.out.println("Objetos: " + humano1.equals(humano2));
    }
}

class Humano {
    String nombres;
    int edad;

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    @Override
    public String toString() {
        return "Humano{" + "nombres=" + nombres + ", edad=" + edad + '}';
    }
}
