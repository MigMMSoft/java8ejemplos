package cl.mmsoft.defaultMethod.java7;

/**
 *
 * @author MigSoft
 */
public class Sumador extends SumadorPrima {
    private final int valorA;
    private final int valorB;

    public Sumador(int a, int b) {
        this.valorA = a;
        this.valorB = b;
    }

    @Override
    public int getValorA() {
        return valorA;
    }

    @Override
    public int getValorB() {
        return valorB;
    }
}
