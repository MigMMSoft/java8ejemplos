package cl.mmsoft.defaultMethod.java7;

/**
 *
 * @author MigSoft
 */
public interface SumaDivideInterface {
    public int getValorA();
    public int getValorB();
    public int suma();
    public int divide();
}
