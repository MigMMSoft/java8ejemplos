package cl.mmsoft.defaultMethod.java7;

/**
 * Este ejemplo es un comparación del ejemplo anterior (Ejemplo01DefaultMethod) 
 * pero utilizando la implementación de java 7 o inferior.
 * 
 * @author MigSoft
 */
public class Ejemplo01DefaultMethod {

    public static void main(String args[]) {
        Sumador operaciones = new Sumador(8,4);
        
        System.err.println("Suma: " + operaciones.suma());
        System.err.println("Multiplica: " + operaciones.multiplica());
        System.err.println("Divide: " + operaciones.divide());
    }
}
