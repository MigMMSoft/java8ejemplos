package cl.mmsoft.defaultMethod.java7;

/**
 *
 * @author MigSoft
 */
public abstract class SumadorPrima extends SumadorAbstract {
    @Override
    public int suma() {
        return getValorA()+getValorB();
    }
}
