package cl.mmsoft.defaultMethod.java7;

/**
 *
 * @author MigSoft
 */
public abstract class SumadorAbstract implements SumaMultiplica, SumaDivideInterface {
    
    
    @Override
    public int divide(){
        return getValorA()/getValorB();
    }
    
    @Override
    public int multiplica(){
        return getValorA()*getValorB();
    }
}
