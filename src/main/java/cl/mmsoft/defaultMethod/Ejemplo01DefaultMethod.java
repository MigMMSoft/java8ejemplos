package cl.mmsoft.defaultMethod;

/**
 * Ejemplo de implementación de interfaces con métodos default. Para el ejemplo 
 * se definen dos interfaces con métodos que se repiten (suma) y un método extra 
 * en cada uno que realiza otra operación
 * @author MigSoft
 */
public class Ejemplo01DefaultMethod {

    public static void main(String args[]) {
        Sumador operaciones = new Sumador(8,4);
        
        System.err.println("Suma: " + operaciones.suma());
        System.err.println("Multiplica: " + operaciones.multiplica());
        System.err.println("Divide: " + operaciones.divide());
    }
}
