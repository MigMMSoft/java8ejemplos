package cl.mmsoft.defaultMethod;

/**
 *
 * @author MigSoft
 */
public interface SumaMultiplica {

    public int getValorA();
    public int getValorB();

    default int suma() {
        return getValorA()+getValorB();
    }
    
    default int multiplica(){
        return getValorA()*getValorB();
    }
}
