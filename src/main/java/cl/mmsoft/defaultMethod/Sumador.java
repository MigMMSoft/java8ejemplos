package cl.mmsoft.defaultMethod;

/**
 *
 * @author MigSoft
 */
public class Sumador implements SumaMultiplica, SumaDivideInterface {
    private final int valorA;
    private final int valorB;

    public Sumador(int a, int b) {
        this.valorA = a;
        this.valorB = b;
    }

    @Override
    public int getValorA() {
        return valorA;
    }

    @Override
    public int getValorB() {
        return valorB;
    }

    @Override
    public int suma() {
        return SumaDivideInterface.super.suma();
        //return SumaMultiplica.super.suma();
    }
}
