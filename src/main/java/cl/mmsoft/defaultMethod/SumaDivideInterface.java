package cl.mmsoft.defaultMethod;

/**
 *
 * @author MigSoft
 */
public interface SumaDivideInterface {
    
    public int getValorA();
    public int getValorB();

    default int suma() {
        return getValorA()+getValorB();
    }
    
    default int divide(){
        return getValorA()/getValorB();
    }
}
