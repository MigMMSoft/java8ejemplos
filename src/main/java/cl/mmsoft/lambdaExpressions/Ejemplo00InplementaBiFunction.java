package cl.mmsoft.lambdaExpressions;

import java.util.function.ToDoubleBiFunction;

/**
 * ToDoubleBiFunction <T, U>
 * Representa una función que acepta dos argumentos y produce un resultado 
 * de doble valor.
 * @author MigSoft
 */
public class Ejemplo00InplementaBiFunction {
    
    /**
     * @param args 
     */
   public static void main(String args[]) {
        Ejemplo00InplementaBiFunction tester = new Ejemplo00InplementaBiFunction();

        ToDoubleBiFunction<Double, Double> suma = (a, b) -> a + b;

        ToDoubleBiFunction<Double, Double> resta = (Double a, Double b) -> a - b;

        ToDoubleBiFunction<Double, Double> multiplica = (Double a, Double b) -> {
            return a * b;
        };

        ToDoubleBiFunction<Double, Double> divide = (a, b) -> a / b;

        System.out.println("10 + 5 = " + tester.calcular(10.0, 5.0, suma));
        System.out.println("10 - 5 = " + tester.calcular(10.0, 5.0, resta));
        System.out.println("10 x 5 = " + tester.calcular(10.0, 5.0, multiplica));
        System.out.println("10 / 5 = " + tester.calcular(10.0, 5.0, divide));
    }

    private Double calcular(Double a, Double b, ToDoubleBiFunction mathOperation) {
        return mathOperation.applyAsDouble(a, b);
    } 
}
