package cl.mmsoft.lambdaExpressions;

/**
 * Este ejemplo es un comparación del ejemplo anterior (Ejemplo01InplementaInterface) 
 * sin la utilización de Lambda 
 * @author MigSoft
 */
public class Ejemplo02InplementaInterfaceSinLambda {

    public static void main(String args[]) {
        Ejemplo02InplementaInterfaceSinLambda tester = new Ejemplo02InplementaInterfaceSinLambda();

        Suma suma = new Suma();

        Resta resta = new Resta();

        Multiplica multiplica = new Multiplica();

        Divide divide = new Divide();

        System.out.println("10 + 5 = " + tester.calcular(10, 5, suma));
        System.out.println("10 - 5 = " + tester.calcular(10, 5, resta));
        System.out.println("10 x 5 = " + tester.calcular(10, 5, multiplica));
        System.out.println("10 / 5 = " + tester.calcular(10, 5, divide));
    }

    private int calcular(int a, int b, OperacionMatematicaInterface mathOperation) {
        return mathOperation.operacion(a, b);
    }
}

class Suma implements OperacionMatematicaInterface{
    @Override
    public int operacion(int a, int b) {
        return a + b;
    }
}

class Resta implements OperacionMatematicaInterface{
    @Override
    public int operacion(int a, int b) {
        return a - b;
    }
}

class Multiplica implements OperacionMatematicaInterface{
    @Override
    public int operacion(int a, int b) {
        return a * b;
    }
}

class Divide implements OperacionMatematicaInterface{
    @Override
    public int operacion(int a, int b) {
        return a / b;
    }
}