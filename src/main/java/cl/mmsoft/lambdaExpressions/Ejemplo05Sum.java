package cl.mmsoft.lambdaExpressions;

import java.util.stream.IntStream;

/**
 * Ejemplo de suma de múltiplos de 3 y 6 en una lista de 1000 numeros usando stream
 * @author MigSoft
 */
public class Ejemplo05Sum {
    public static void main(String args[]) {
        int sumaMultiplos = IntStream.
                range(1, 1000).
                filter(numero -> numero % 3 == 0 || numero % 6 == 0).
                sum();
        
        System.err.println("Multiplos 3: " + sumaMultiplos); 
    }
}
