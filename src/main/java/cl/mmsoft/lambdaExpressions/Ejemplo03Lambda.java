package cl.mmsoft.lambdaExpressions;

import java.util.Map;
import java.util.TreeMap;

/**
 * Ejemplo de Lambda con llamado a una función espesifica
 * @author MigSoft
 */
public class Ejemplo03Lambda {
    public static void main(String args[]) {
        Ejemplo03Lambda lambda = new Ejemplo03Lambda();
        Map<String, String> mapList = new TreeMap();

        mapList.put("1", "Mahesh");
        mapList.put("2", "Suresh");
        mapList.put("3", "Ramesh");
        mapList.put("4", "Naresh");
        mapList.put("5", "Kalpesh");

        mapList.forEach((clave, valor) -> lambda.print(clave, valor));
    }
    
    public void print(String key, String value) {
        System.out.println("Key: " + key + " Value: " + value);
    }
}
