package cl.mmsoft.lambdaExpressions;

/**
 *
 * @author MigSoft
 */
public interface OperacionMatematicaInterface {
    int operacion(int a, int b);
}
