package cl.mmsoft.lambdaExpressions;

/**
 * Ejemplo de definición de variables e distintos contextos. Contextos tales 
 * como: de clase, de parámetros,  del Lambda.
 * @author MigSoft
 */
import java.util.function.Consumer;

public class Ejemplo04Scope {
    //variable case principal
    public int x = 0;
    
    public static void main(String... args) {
        Ejemplo04Scope st = new Ejemplo04Scope();
        Ejemplo04Scope.NivelUno fl = st.new NivelUno();
        fl.metodoNivelUno(23);
    }

    class NivelUno {
        //variable clase inerclass
        public int x = 1;
        
        //variable del metodo
        void metodoNivelUno(int x) {
            Consumer<Integer> myConsumer = (xx)
                    -> {
                System.out.println("x = " + x); // Statement A
                System.out.println("xx = " + xx);
                System.out.println("this.x = " + this.x);
                System.out.println("Ejemplo04Scope.this.x = " + Ejemplo04Scope.this.x);
            };
            
            myConsumer.accept(x+5);
        }
    }
}