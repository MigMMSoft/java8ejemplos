package cl.mmsoft.lambdaExpressions;

/**
 * OperacionMatematicaInterface es una interface custom similar a ToDoubleBiFunction
 * pero los parametos y la respuesta son de tipo int
 * @author MigSoft
 */
public class Ejemplo01InplementaInterface {

    public static void main(String args[]) {
        Ejemplo01InplementaInterface tester = new Ejemplo01InplementaInterface();

        OperacionMatematicaInterface suma = (int a, int b) -> a + b;

        OperacionMatematicaInterface resta = (a, b) -> a - b;

        OperacionMatematicaInterface multiplica = (int a, int b) -> {
            return a * b;
        };

        OperacionMatematicaInterface divide = (int a, int b) -> a / b;

        System.out.println("10 + 5 = " + tester.calcular(10, 5, suma));
        System.out.println("10 - 5 = " + tester.calcular(10, 5, resta));
        System.out.println("10 x 5 = " + tester.calcular(10, 5, multiplica));
        System.out.println("10 / 5 = " + tester.calcular(10, 5, divide));
    }

    private int calcular(int a, int b, OperacionMatematicaInterface mathOperation) {
        return mathOperation.operacion(a, b);
    }
}
