package cl.mmsoft.dataTimeAPI;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.Month;

/**
 * ejemplo de LocalDate / LocalTime y LocalDateTime
 * @author MigSoft
 */
public class Ejemplo01Local {

    /**
     * 
     *
     * @param args
     */
    public static void main(String args[]) {
        Ejemplo01Local localDataTime = new Ejemplo01Local();
        localDataTime.testLocalDateTime();
        localDataTime.testLocalDate();
        localDataTime.testLocalTime();
    }
    
    public void testLocalDateTime(){
        // Get the current date and time
        LocalDateTime currentTime = LocalDateTime.now();
        System.out.println("Dia actual: " + currentTime);

        LocalDate date1 = currentTime.toLocalDate();
        System.out.println("toLocalDate: " + date1);

        Month month = currentTime.getMonth();
        int day = currentTime.getDayOfMonth();
        int seconds = currentTime.getSecond();

        System.out.println("Mes: " + month + " Dia: " + day + " Segundo: " + seconds);

        LocalDateTime date2 = currentTime.withDayOfMonth(10).withYear(2012);
        System.out.println("withDayOfMonth: " + date2);
    }
    
    public void testLocalDate() {
        //12 december 2014
        LocalDate date3 = LocalDate.of(2014, Month.DECEMBER, 12);
        System.out.println("of: " + date3);
    }

    public void testLocalTime() {
        //22 hour 15 minutes
        LocalTime date4 = LocalTime.of(22, 15);
        System.out.println("of: " + date4);

        //parse a string
        LocalTime date5 = LocalTime.parse("20:15:30.564");
        System.out.println("parse: " + date5);

        LocalTime date6 = LocalTime.parse("20:15");
        System.out.println("parse: " + date6);
        
//        Date a = new Date();
//        LocalTime date7 = LocalTime.ofNanoOfDay(a.getTime());
//        System.out.println("parse: " + date7);
    }
}
