package cl.mmsoft.dataTimeAPI;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import java.util.Date;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.time.temporal.ChronoField;

/**
 * Se agrega un método toInstant () a los objetos Date y Calendar originales,
 * que se pueden utilizar para convertirlos a la nueva API de fecha y hora.
 * Utilice un método ofInstant (Insant, ZoneId) para obtener un objeto
 * LocalDateTime o ZonedDateTime
 *
 * @author MigSoft
 */
public class Ejemplo06BackwardCompatibility {

    /**
     * @param args
     */
    public static void main(String args[]) {
        Ejemplo06BackwardCompatibility backwardCompatibility = new Ejemplo06BackwardCompatibility();
        backwardCompatibility.testDateToInstant();
        System.out.println("_______________________________________");
        backwardCompatibility.testLocalToDate();
    }
    

    public void testDateToInstant() {
        //Get the current date
        Date currentDate = new Date();
        System.out.println("Fecha actual: " + currentDate);

        //Get the instant of current date in terms of milliseconds
        Instant now = currentDate.toInstant();
        ZoneId currentZone = ZoneId.systemDefault();

        LocalDateTime localDateTime = LocalDateTime.ofInstant(now, currentZone);
        System.out.println("Local date: " + localDateTime);

        ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant(now, currentZone);
        System.out.println("Zoned date: " + zonedDateTime);
    }

    public void testLocalToDate() {
        LocalDate localDate = LocalDate.of(2015, Month.JULY, 4);
        System.out.println("LocalDate -> Date: " + asDate(localDate));
        
        LocalDateTime currentTime = LocalDateTime.now();
        System.out.println("LocalDateTime -> Date: " + asDate(currentTime));
    }

    private Date asDate(LocalDate localDate) {
        ZoneId currentZone = ZoneId.systemDefault();
        Instant instant = localDate.atStartOfDay().atZone(currentZone).toInstant();
        return Date.from(instant);
    }

    private Date asDate(LocalDateTime localDateTime) {
        ZoneId currentZone = ZoneId.systemDefault();
        Instant instant = localDateTime.atZone(currentZone).toInstant();
        return Date.from(instant);
    }
}
