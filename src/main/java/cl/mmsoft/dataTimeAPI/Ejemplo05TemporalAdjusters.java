package cl.mmsoft.dataTimeAPI;

import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.time.DayOfWeek;

/**
 * TemporalAdjuster se utiliza para realizar las matemáticas de la fecha.
 * Por ejemplo, obtenga el "segundo sábado del mes" o "el próximo martes"
 * @author MigSoft
 */
public class Ejemplo05TemporalAdjusters {

    /**
     * @param args
     */
    public static void main(String args[]) {
        Ejemplo05TemporalAdjusters temporalAdjusters = new Ejemplo05TemporalAdjusters();
        temporalAdjusters.testAdjusters();
    }

    public void testAdjusters() {
        //Get the current date
        LocalDate date1 = LocalDate.now();
        System.out.println("Fecha actual: " + date1);

        //get the next tuesday
        LocalDate nextTuesday = date1.with(TemporalAdjusters.next(DayOfWeek.TUESDAY));
        System.out.println("Siguiente martes : " + nextTuesday);

        //get the second saturday of next month
        LocalDate firstInYear = LocalDate.of(date1.getYear(), date1.getMonth(), 1);
        System.out.println("Siguiente : " + firstInYear);
        LocalDate secondSaturday = firstInYear.with(TemporalAdjusters.nextOrSame(DayOfWeek.SATURDAY)).with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
        System.out.println("El sabado sub siguiente : " + secondSaturday);
    }
}
