package cl.mmsoft.dataTimeAPI;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Duration;
import java.time.Period;
import java.time.temporal.ChronoUnit;

/**
 * Con Java 8, se introducen dos clases especializadas para hacer frente a 
 * las diferencias de tiempo:
 * 
 * Período - Se refiere a la cantidad de tiempo basada en la fecha.
 * 
 * Duración - Se trata de tiempo basado en la cantidad de tiempo.
 *  
 * @author MigSoft
 */
public class Ejemplo04PeriodDuration {
    /**
     * @param args 
     */
    public static void main(String args[]) {
        Ejemplo04PeriodDuration periodDuration = new Ejemplo04PeriodDuration();
        periodDuration.testPeriod();
        periodDuration.testDuration();
    }

    public void testPeriod() {

        //Get the current date
        LocalDate date1 = LocalDate.now();
        System.out.println("Dia actual: " + date1);

        //add 1 month to the current date
        LocalDate date2 = date1.plus(1, ChronoUnit.MONTHS);
        System.out.println("Siguiente mes: " + date2);

        Period period = Period.between(date2, date1);
        System.out.println("Period: " + period);
    }

    public void testDuration() {
        LocalTime time1 = LocalTime.now();
        System.out.println("Hora Actual: " + time1);
        
        LocalTime time2 = time1.plus(2, ChronoUnit.HOURS);
        System.out.println("suma 2 horas con ChronoUnit: " + time2);
        
        Duration duration = Duration.between(time1, time2);
        System.out.println("Duration: " + duration);
        
        Duration twoHours = Duration.ofHours(2);
        LocalTime time3 = time1.plus(twoHours);
        System.out.println("suma 2 horas con Duration: " + time1);
        Duration duration2 = Duration.between(time1, time3);
        System.out.println("Duration: " + duration2);
    }
}
