package cl.mmsoft.dataTimeAPI;

import java.time.ZonedDateTime;
import java.time.ZoneId;

/**
 * La API de fecha y hora de zona se utilizará cuando se considere la zona
 * horaria.
 * @author MigSoft
 */
public class Ejemplo02Zoned {

    /**
     *
     * @param args
     */
    public static void main(String args[]) {
        Ejemplo02Zoned zonedDateTime = new Ejemplo02Zoned();
        zonedDateTime.testZonedDateTime();
    }

    public void testZonedDateTime() {
        // Get the current date and time
        ZonedDateTime date1 = ZonedDateTime.parse("2007-12-03T10:15:30+05:30[Asia/Karachi]");
        System.out.println("date1: " + date1);

        ZoneId id = ZoneId.of("America/Santiago");
        System.out.println("ZoneId: " + id);

        ZoneId currentZone = ZoneId.systemDefault();
        System.out.println("CurrentZone: " + currentZone);

        ZonedDateTime date2 = date1.withZoneSameLocal(id);
        System.out.println("date2: " + date2);
    }
}
