package cl.mmsoft.dataTimeAPI;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 *Java.time.temporal.ChronoUnit enum se agrega en Java 8 para reemplazar
 * los valores enteros usados en la API antigua para representar día, mes,
 * etc.
 * @author MigSoft
 */
public class Ejemplo03ChronoUnits {

    /**
     * @param args
     */
    public static void main(String args[]) {
        Ejemplo03ChronoUnits chronoUnits = new Ejemplo03ChronoUnits();
        chronoUnits.testChromoUnits();
    }

    public void testChromoUnits() {

        //Get the current date
        LocalDate today = LocalDate.now();
        System.out.println("Dia actual: " + today);
        System.out.println("--------------------------");
        LocalDate nextDays = today.plus(1, ChronoUnit.DAYS);
        System.out.println("Siguiente dia: " + nextDays);

        //add 1 week to the current date
        LocalDate nextWeek = today.plus(1, ChronoUnit.WEEKS);
        System.out.println("Siguiente semana: " + nextWeek);

        //add 1 month to the current date
        LocalDate nextMonth = today.plus(1, ChronoUnit.MONTHS);
        System.out.println("Siguiente mes: " + nextMonth);

        //add 1 year to the current date
        LocalDate nextYear = today.plus(1, ChronoUnit.YEARS);
        System.out.println("Siguiente año: " + nextYear);

        //add 10 years to the current date
        LocalDate nextDecade = today.plus(1, ChronoUnit.DECADES);
        System.out.println("Siguiente decada: " + nextDecade);
    }
}
