package cl.mmsoft.methodReference;

import java.util.Map;
import java.util.TreeMap;

/**
 * Ejemplo de llamado por referencia a método de una instancia de objeto
 * @author MigSoft
 */
public class Ejemplo02InstanceReference {

    public static void main(String args[]) {
        Ejemplo02InstanceReference instanceReference = new Ejemplo02InstanceReference();

        instanceReference.printMap();
    }

    public void printMap() {
        Map<String, String> mapList = new TreeMap();

        mapList.put("1", "Mahesh");
        mapList.put("2", "Suresh");
        mapList.put("3", "Ramesh");
        mapList.put("4", "Naresh");
        mapList.put("5", "Kalpesh");
       
        Ejemplo02InstanceReference instanceReference = new Ejemplo02InstanceReference();
        mapList.forEach(instanceReference::print);
    }

    public void print(String key, String value) {
        System.out.println("Key: " + key + " Value: " + value);
    }
}
