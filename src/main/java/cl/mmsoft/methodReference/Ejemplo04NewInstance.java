package cl.mmsoft.methodReference;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.function.Supplier;

/**
 * Ejemplo de creación de objeto por referencia (::new) utilizando Supplier
 * @author MigSoft
 */
public class Ejemplo04NewInstance {

    public static void main(String args[]) {
        ArrayList<ClaseTest> listaTest = new ArrayList();
        listaTest.add(new ClaseTest("1"));
        listaTest.add(new ClaseTest("2"));
        listaTest.add(new ClaseTest("3"));

        Collection<ClaseTest> listaCopia = copiaElementos(listaTest, HashSet::new);

        System.out.println("Lista original 1 " + listaTest.hashCode());
        listaTest.
                stream().
                sorted((h1, h2) -> h1.getDato().compareTo(h2.getDato())).
                forEach(System.out::println);
        
        System.out.println("Lista copia 1 " + listaCopia.hashCode());
        listaCopia.
                stream().
                sorted((h1, h2) -> h1.getDato().compareTo(h2.getDato())).
                forEach(System.out::println);
        
    }

    public static <T, SOURCE extends Collection<T>, DEST extends Collection<T>>
        DEST copiaElementos(
                    SOURCE origenCollection,
                    Supplier<DEST> destinoCollection) {

        DEST result = destinoCollection.get();
        origenCollection.forEach((T t) -> {
            result.add(t);
        });
        return result;
    }
}

class ClaseTest{
    String dato;
    public ClaseTest(ClaseTest t){
        this.dato = t.dato;
    }
    
    public ClaseTest(String dato){
        this.dato=dato;
    }

    public String getDato() {
        return dato;
    }

    public void setDato(String dato) {
        this.dato = dato;
    }
    
    @Override
    public String toString(){
        return this.dato + " hasCode: " + this.hashCode();
    }
}
