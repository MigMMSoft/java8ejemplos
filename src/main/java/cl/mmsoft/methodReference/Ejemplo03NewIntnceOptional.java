package cl.mmsoft.methodReference;

import java.util.Optional;

/**
 * Ejemplo de creación de objeto por referencia (::new) utilizando Optional
 * @author mig_s
 */
public class Ejemplo03NewIntnceOptional {

    public static void main(String args[]) {
        Ejemplo03NewIntnceOptional ejemplo04 = new Ejemplo03NewIntnceOptional();
        
        try {
            System.out.println("Valida texto: " + ejemplo04.testStringNull("texto"));
            System.out.println("Valida texto: " + ejemplo04.testStringNull(null));
        } catch (CustomException ex) {
            System.out.println("Error: " + ex.getMessage());
        }

    }

    public String testStringNull(String value) throws CustomException {
        Optional<String> valueOpt = Optional.ofNullable(value);
        String result = valueOpt.orElseThrow(CustomException::new).toUpperCase();
        return result;
    }
}

class CustomException extends Exception{
    public CustomException(){
        super("El valor es nulo");
    }
}