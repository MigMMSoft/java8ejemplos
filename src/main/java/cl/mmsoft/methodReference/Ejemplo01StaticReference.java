package cl.mmsoft.methodReference;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Ejemplo de llamado por referencia a método estatica
 * @author MigSoft
 */
public class Ejemplo01StaticReference {

    public static void main(String args[]) {
        Ejemplo01StaticReference staticReference = new Ejemplo01StaticReference();
        
        staticReference.printList();
        staticReference.printMap();
    }

    public void printList() {
        List names = new ArrayList();

        names.add("Mahesh");
        names.add("Suresh");
        names.add("Ramesh");
        names.add("Naresh");
        names.add("Kalpesh");

        names.forEach(System.out::println);
    }

    public void printMap() {
        Map<String,String> mapList = new TreeMap();
        
        mapList.put("1","Mahesh");
        mapList.put("2","Suresh");
        mapList.put("3","Ramesh");
        mapList.put("4","Naresh");
        mapList.put("5","Kalpesh");

        mapList.forEach(Ejemplo01StaticReference::print);
        //mapList.forEach(System.out::println);
    }
    
    public static void print(String key, String value){
        System.out.println("Key: " + key + " Value: " + value);
    }
}
