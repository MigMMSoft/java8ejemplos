package cl.mmsoft.methodReference;

import com.google.common.collect.Lists;
import java.util.Comparator;
import java.util.List;

/**
 * Ejemplo de llamado por referencia a método de una instancia de otro objeto 
 * utilizando Comparator
 * @author MigSoft
 */
public class Ejemplo05DeLaintanciaDeOtroObjeto {

    public static void main(String args[]) {
        List<Humano> humans = Lists.newArrayList(
                new Humano("Sarah", 12),
                new Humano("arah", 10),
                new Humano("Zack", 13)
        );

        humans.sort(
                Comparator.
                comparing(Humano::getNombre).thenComparing(Humano::getEdad)
        );
        
        humans.stream().sorted(
                Comparator.
                comparing(Humano::getEdad)
        ).forEach(System.out::println);
        
        humans.stream().forEach(System.out::println);
    }
}

class Humano{
    private String nombre;
    private Integer edad;
    public Humano(String nombre, Integer edad){
        this.nombre=nombre;
        this.edad=edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }
    
    @Override
    public String toString(){
        return "Nombre: "+nombre+" edad:"+edad;
    }
}